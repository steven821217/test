
Function FileExists(FilePath)
  Set fso = CreateObject("Scripting.FileSystemObject")
  If fso.FileExists(FilePath) Then
    FileExists=CBool(1)
  Else
    FileExists=CBool(0)
  End If
End Function


Url = "https://drive.google.com/u/0/uc?id=1ZYppnQh_jVatYAHNn0YGDr7Ubg31_n2n&export=download"
Set objpath = createobject("Scripting.FileSystemObject")
currentpath = objpath.GetFolder(".").Path
textfilepath = objpath.BuildPath(currentpath, "download.txt")


If FileExists(textFilePath) Then
  WScript.Echo textfilepath & " already Exist"
Else
  WScript.Echo textfilepath & " file does not exist and will be downloaded"
  dim xHttp: Set xHttp = createobject("MSXML2.ServerXMLHTTP")
  dim bStrm: Set bStrm = createobject("Adodb.Stream")
  xHttp.Open "GET", Url, False
  xHttp.Send
    
  with bStrm
      .type = 1 
      .open
      .write xHttp.responseBody
      .savetofile textFilePath, 2 
  end with
End If
 


